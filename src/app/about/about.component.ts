import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  public date: Date;


  public minutes: number;
  constructor() { }

  ngOnInit() {
    this.date = new Date();
    this.minutes = 0;
  }

}
