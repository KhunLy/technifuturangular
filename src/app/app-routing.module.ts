import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { TokenGuardGuard } from './core/_guards/token-guard.guard';
import { LoginComponent } from './shared/_components/login/login.component';

const routes : Route[] = [
    { path: "home", component: HomeComponent },
    { path: "about", component: AboutComponent, canActivate: [TokenGuardGuard] },
    { path: "login", component: LoginComponent },
    { path: 'demo', loadChildren: () => import('./features/demo/demo.module').then(m => m.DemoModule) },
    { path: 'ex', loadChildren: () => import('./features/ex/ex.module').then(m => m.ExModule) },
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule{

}