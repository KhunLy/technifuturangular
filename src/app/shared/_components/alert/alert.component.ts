import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  
  private _title : string;
  public get title() : string {
    return this._title;
  }
  @Input() public set title(v : string) {
    this._title = v;
  }

  
  private _color : string;
  public get color() : string {
    return this._color;
  }
  @Input() public set color(v : string) {
    this._color = v;
  }

  
  private _time : number;
  public get time() : number {
    return this._time;
  }
  public set time(v : number) {
    this._time = v;
  }


  
  private _isDismissed : boolean;
  public get isDismissed() : boolean {
    return this._isDismissed;
  }
  public set isDismissed(v : boolean) {
    this._isDismissed = v;
  }
  

  
  private _close : EventEmitter<number>;
  @Output() public get close() : EventEmitter<number> {
    return this._close;
  }
  

  constructor() {
    this._close = new EventEmitter<number>();

  }

  ngOnInit() {
    this.time = (new Date()).getTime();

  }

  onDismiss() {
    this.isDismissed = true;
    let currentTime = (new Date()).getTime();
    this.close.emit(currentTime - this.time);
  }

}
