import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from 'src/app/core/_services/login.service';
import { SessionService } from 'src/app/core/_services/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public form: FormGroup;
  constructor(
    private loginService: LoginService,
    private session: SessionService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.form= new FormGroup({
      "email": new FormControl(null, Validators.compose([
        Validators.email,
        Validators.required
      ])),
      "password": new FormControl(null, Validators.compose([
        Validators.required
      ])),
    })
  }

  submit(email: string, password: string) {
    //afficher un loader
    this.loginService.login(email, password)
      .subscribe(data => {
        this.session.saveToken(data);
        this.router.navigateByUrl('/home');
        // stocker le token dans la session
      }, error => {
        //afficher un message d'erreur
        //cacher le loader
      }, () => {
        // cacher le loader
      });
  }
}
