import { Component, OnInit } from '@angular/core';
import { LoaderMediatorService } from 'src/app/core/_services/loader-mediator.service';

@Component({
  selector: 'shared-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  private _isVisible : boolean;
  public get isVisible() : boolean {
    return this._isVisible;
  }
  public set isVisible(v : boolean) {
    this._isVisible = v;
  }
  

  constructor(private mediator: LoaderMediatorService) { }

  ngOnInit() {
    this.isVisible = false;
    this.mediator.loaderState.subscribe( data => {
      this.isVisible = data;
    })
  }

}
