import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  
  private _title : string;
  public get title() : string {
    return this._title;
  }
  public set title(v : string) {
    this._title = v;
  }
  

  constructor(private ref: NbDialogRef<ConfirmDialogComponent>) { }

  ngOnInit() {
  }

  yes() {
    this.ref.close(true);
  }

  no() {
    this.ref.close(false);
  }

}
