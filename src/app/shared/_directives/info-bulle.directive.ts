import { Directive, ElementRef, Input, Inject, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Directive({
  selector: '[appInfoBulle]'
})
export class InfoBulleDirective {

  @Input() public appInfoBulle: string;

  private div: any;

  @HostListener('mousemove', ['$event']) onMouseOver($event) {
    this.div.style.display = 'block';
    this.div.innerHTML = this.appInfoBulle;
    this.div.style.top = $event.clientY - 20 + "px";
    this.div.style.left = $event.clientX + "px";
  }

  @HostListener('mouseleave') onMouseLeave($event) {
    this.div.style.display = 'none';
  }

  constructor(
    private ref: ElementRef, 
    @Inject(DOCUMENT) private doc: Document
  ) { 
    this.div = doc.createElement("div");
    this.div.style.color = "black";
    this.div.style.border = "1px solid black";
    this.div.style.position = "absolute";
    this.div.style.display = "none";
    // div.style.bottom = "0";
    // div.style.left = "0";
    this.div.style.background = "white";
    ref.nativeElement.appendChild(this.div);
    //ref.nativeElement.style.position = "relative"; 
    
  }

}
