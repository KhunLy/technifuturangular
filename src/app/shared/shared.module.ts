import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbLayoutModule, NbButtonModule, NbCardModule, NbSelectModule, NbInputModule, NbCheckboxModule, NbIconModule, NbListModule, NbDatepickerModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { FormsModule } from '@angular/forms';
import { ConfirmDialogComponent } from './_components/confirm-dialog/confirm-dialog.component';
import { HttpClientModule } from '@angular/common/http';
import { LoaderComponent } from './_components/loader/loader.component';
import { AlertComponent } from './_components/alert/alert.component';
import { InfoBulleDirective } from './_directives/info-bulle.directive';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './_components/login/login.component';

@NgModule({
  declarations: [ConfirmDialogComponent, 
    LoaderComponent, AlertComponent, InfoBulleDirective, LoginComponent],
  imports: [
    CommonModule,
    NbLayoutModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbCardModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    NbIconModule,
    NbListModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  entryComponents: [ ConfirmDialogComponent ],
  exports: [
    NbLayoutModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbCardModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    NbIconModule,
    NbListModule,
    FormsModule,
    ConfirmDialogComponent,
    HttpClientModule,
    LoaderComponent,
    AlertComponent,
    InfoBulleDirective,
    ReactiveFormsModule,

    LoginComponent,
  ]
})
export class SharedModule { }
