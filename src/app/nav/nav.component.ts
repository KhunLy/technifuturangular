import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { SessionService } from '../core/_services/session.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  
  
  private _items : NbMenuItem[];
  public get items() : NbMenuItem[] {
    return this._items;
  }
  public set items(v : NbMenuItem[]) {
    this._items = v;
  }
  

  constructor(
    private sessionService: SessionService
  ) { }

  ngOnInit() {
    
    let onConnectList = [
      { title: "Home", icon: "home", link: "/home" },
      { title: "About", icon: "question-mark", link: "/about" },
      { title: "Google", icon: "google", url: "https://google.be", target: "_blank" },
      { title: "Demo", icon: "book", children: [
        { title: "Demo 1 - Binding 1 Way", link: "/demo/demo1" },
        { title: "Demo 2 - Events", link: "/demo/demo2" },
        { title: "Demo 3 - *ngIf", link: "/demo/demo3" },
        { title: "Demo 4 - *ngFor", link: "/demo/demo4" },
        { title: "Demo 5 - Binding 2 Ways", link: "/demo/demo5" },
        { title: "Demo 6 - Observables", link: "/demo/demo6" },
        { title: "Demo 7 - Meteo", link: "/demo/demo7" },
        { title: "Demo 8 - Formulaire", link: "/demo/demo8" },
      ] },
      { title: "Exercice", icon: "folder", children: [
        { title: "Exo 1 - Shopping List", link: "/ex/ex1" },
        { title: "Exo 2 - Marvel API", link: "/ex/ex2" },
        { title: "Exo 3 - Master - details", link: "/ex/ex3" },
      ]}
    ];

    let onDiscConnList = [
      { title: "Home", icon: "home", link: "/home" },
      { title: "Login", icon: "star", link: "/login" },
      { title: "Google", icon: "google", url: "https://google.be", target: "_blank" },
      { title: "Demo", icon: "book", children: [
        { title: "Demo 1 - Binding 1 Way", link: "/demo/demo1" },
        { title: "Demo 2 - Events", link: "/demo/demo2" },
        { title: "Demo 3 - *ngIf", link: "/demo/demo3" },
        { title: "Demo 4 - *ngFor", link: "/demo/demo4" },
        { title: "Demo 5 - Binding 2 Ways", link: "/demo/demo5" },
        { title: "Demo 6 - Observables", link: "/demo/demo6" },
        { title: "Demo 7 - Meteo", link: "/demo/demo7" },
        { title: "Demo 8 - Formulaire", link: "/demo/demo8" },
      ] },
      { title: "Exercice", icon: "folder", children: [
        { title: "Exo 1 - Shopping List", link: "/ex/ex1" },
        { title: "Exo 2 - Marvel API", link: "/ex/ex2" },
        { title: "Exo 3 - Master - details", link: "/ex/ex3" },
      ]}
    ]
    let currentToken = this.sessionService.getToken();
    this.items = currentToken == null ? onDiscConnList : onConnectList;
    this.sessionService.sessionToken$.subscribe(data => 
      {
        this.items = data == null ? onDiscConnList : onConnectList;
      }
    );
  }

}
