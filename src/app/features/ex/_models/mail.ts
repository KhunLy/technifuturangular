export interface Mail {
    subject: string;
    date: Date;
    content: string;
}