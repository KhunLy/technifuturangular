import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExComponent } from './ex.component';
import { Ex1Component } from './ex1/ex1.component';
import { Ex2Component } from './ex2/ex2.component';
import { Ex2detailsComponent } from './ex2details/ex2details.component';
import { Ex3Component } from './ex3/ex3.component';
import { CharacterService } from 'src/app/core/_services/character.service';
import { CharacterResolverService } from 'src/app/core/_resolvers/character-resolver.service';

const routes: Routes = [{ path: '', component: ExComponent, children: [
  { path: 'ex1', component: Ex1Component },
  { path: 'ex2', component: Ex2Component },
  { path: 'ex2details/:id', component: Ex2detailsComponent,
    resolve: { character: CharacterResolverService }
 },
  { path: 'ex3', component: Ex3Component },
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExRoutingModule { }
