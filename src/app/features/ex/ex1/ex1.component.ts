import { Component, OnInit } from '@angular/core';
import { Article } from '../_models/article';
import { NbDialogService } from '@nebular/theme';
import { ConfirmDialogComponent } from 'src/app/shared/_components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-ex1',
  templateUrl: './ex1.component.html',
  styleUrls: ['./ex1.component.scss']
})
export class Ex1Component implements OnInit {

  
  private _name : string;
  public get name() : string {
    return this._name;
  }
  public set name(v : string) {
    this._name = v;
  }

  
  private _articles : Article[];
  public get articles() : Article[] {
    return this._articles;
  }
  public set articles(v : Article[]) {
    this._articles = v;
  }

  constructor(private dialogService: NbDialogService) {}

  ngOnInit() {
    this.articles = [];
  }

  onAdd() {
    this.articles.push(
      { name: this.name, checked: false }
    );
    this.name = null;
  }

  check(item: Article) {

    //console.log(item);
    
    item.checked = !item.checked;
  }

  deleteItem(item: Article) {
    
    //this.articles = this.articles.filter(a => a != item);
    let ref = this.dialogService.open(ConfirmDialogComponent, {
      context: { title: item.name }
    });
    ref.onClose.subscribe(data => {
      if(data)
        this.articles.splice(this.articles.indexOf(item) ,1);
    });
  }

}
