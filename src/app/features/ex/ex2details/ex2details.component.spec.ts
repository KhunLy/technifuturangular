import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ex2detailsComponent } from './ex2details.component';

describe('Ex2detailsComponent', () => {
  let component: Ex2detailsComponent;
  let fixture: ComponentFixture<Ex2detailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ex2detailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ex2detailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
