import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharacterService } from 'src/app/core/_services/character.service';
import { CharactersApi } from 'src/app/core/_models/characters-api';

@Component({
  selector: 'app-ex2details',
  templateUrl: './ex2details.component.html',
  styleUrls: ['./ex2details.component.scss']
})
export class Ex2detailsComponent implements OnInit {


  
  private _model : CharactersApi;
  public get model() : CharactersApi {
    return this._model;
  }
  public set model(v : CharactersApi) {
    this._model = v;
  }
  

  constructor(
    private router: ActivatedRoute,
    //private charService: CharacterService
  ) { }

  ngOnInit() {
    this.model = this.router.snapshot.data.character;
    // let id = this.router.snapshot.params["id"];
    // this.charService.getById(id).subscribe(data => {
    //   this.model = data;
    // });
  }

}
