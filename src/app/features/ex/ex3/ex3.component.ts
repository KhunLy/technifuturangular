import { Component, OnInit } from '@angular/core';
import { Mail } from '../_models/mail';

@Component({
  selector: 'app-ex3',
  templateUrl: './ex3.component.html',
  styleUrls: ['./ex3.component.scss']
})
export class Ex3Component implements OnInit {

  
  private _currentMail : Mail;
  public get currentMail() : Mail {
    return this._currentMail;
  }
  public set currentMail(v : Mail) {
    this._currentMail = v;
  }
  

  constructor() { }

  ngOnInit() {
  }

  changeEmail(mail: Mail){
    this.currentMail = mail;
  }

}
