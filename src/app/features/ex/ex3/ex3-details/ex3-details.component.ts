import { Component, OnInit, Input } from '@angular/core';
import { Mail } from '../../_models/mail';

@Component({
  selector: 'app-ex3-details',
  templateUrl: './ex3-details.component.html',
  styleUrls: ['./ex3-details.component.scss']
})
export class Ex3DetailsComponent implements OnInit {

  
  private _mail : Mail;
  public get mail() : Mail {
    return this._mail;
  }
  @Input() public set mail(v : Mail) {
    this._mail = v;
  }
  

  constructor() { }

  ngOnInit() {
  }

}
