import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Mail } from '../../_models/mail';

@Component({
  selector: 'app-ex3-master',
  templateUrl: './ex3-master.component.html',
  styleUrls: ['./ex3-master.component.scss']
})
export class Ex3MasterComponent implements OnInit {

  private _mailChanged: EventEmitter<Mail>;
  @Output() get mailChanged() :EventEmitter<Mail> {
    return this._mailChanged;
  }

  private _mails : Mail[];
  public get mails() : Mail[] {
    return this._mails;
  }
  public set mails(v : Mail[]) {
    this._mails = v;
  }
  
  constructor() {
    this._mailChanged = new EventEmitter<Mail>();
  }

  ngOnInit() {
    this.mails = [
      { subject: 'Title 1', date: new Date(), content: '<p>Content 1</p>' },
      { subject: 'Title 2', date: new Date((new Date()).valueOf() - 1000*3600*24), content: '<p>Content 2</p>' },
      { subject: 'Title 3', date: new Date((new Date()).valueOf() - 1000*3600*48), content: '<p>Content 3</p>' },
      { subject: 'Title 4', date: new Date((new Date()).valueOf() - 1000*3600*72), content: '<p>Content 4</p>' },
    ]
  }

  selectMail(mail: Mail){
    this.mailChanged.emit(mail);
  }

}
