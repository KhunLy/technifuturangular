import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExRoutingModule } from './ex-routing.module';
import { ExComponent } from './ex.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Ex1Component } from './ex1/ex1.component';
import { NbDialogModule } from '@nebular/theme';
import { Ex2Component } from './ex2/ex2.component';
import { Ex2detailsComponent } from './ex2details/ex2details.component';
import { Ex3Component } from './ex3/ex3.component';
import { Ex3DetailsComponent } from './ex3/ex3-details/ex3-details.component';
import { Ex3MasterComponent } from './ex3/ex3-master/ex3-master.component';


@NgModule({
  declarations: [ExComponent, Ex1Component, Ex2Component, Ex2detailsComponent, Ex3Component, Ex3DetailsComponent, Ex3MasterComponent],
  imports: [
    CommonModule,
    ExRoutingModule,
    SharedModule,
    NbDialogModule.forChild(),
  ]
})
export class ExModule { }
