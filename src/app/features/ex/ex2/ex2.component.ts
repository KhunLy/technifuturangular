import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CharacterService } from 'src/app/core/_services/character.service';
import { CharactersApi } from 'src/app/core/_models/characters-api';

@Component({
  selector: 'app-ex2',
  templateUrl: './ex2.component.html',
  styleUrls: ['./ex2.component.scss']
})
export class Ex2Component implements OnInit {

  
  private _model : CharactersApi;
  public get model() : CharactersApi {
    return this._model;
  }
  public set model(v : CharactersApi) {
    this._model = v;
  }
  

  constructor(private charService: CharacterService) { 
  }

  ngOnInit() {
    this.charService.getAll()
      //.pipe() plus tard pour mapper
       .subscribe(data => {this.model = data; console.log(data);
       })
  }

}
