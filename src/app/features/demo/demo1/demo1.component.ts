import { Component, OnInit } from '@angular/core';
import { CharacterService } from 'src/app/core/_services/character.service';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component implements OnInit {

  
  private _name : string;
  public get name() : string {
    return this._name;
  }
  public set name(v : string) {
    this._name = v;
  }
  

  constructor(private charService: CharacterService) { }

  ngOnInit() {
    this.name = "John DOE";
    setTimeout(() => { this.name = "Khun LY" }, 3000);
  }

}
