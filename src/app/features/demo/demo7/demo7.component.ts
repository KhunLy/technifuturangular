import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WeatherRequest } from '../_models/weather-request';

@Component({
  selector: 'app-demo7',
  templateUrl: './demo7.component.html',
  styleUrls: ['./demo7.component.scss']
})
export class Demo7Component implements OnInit {


  
  private _obs$ : Observable<WeatherRequest>;
  public get obs$() : Observable<WeatherRequest> {
    return this._obs$;
  }
  public set obs$(v : Observable<WeatherRequest>) {
    this._obs$ = v;
  }
  

  constructor(private client: HttpClient) { }

  ngOnInit() {
    this.obs$ = this.client.get<WeatherRequest>(
      "http://api.openweathermap.org/data/2.5/weather?lat=50.63&lon=5.58&APPID=d52e50e34214ff0b92247f788638eeb9"
    );
  }

}
