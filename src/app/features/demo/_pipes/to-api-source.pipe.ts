import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toApiSource'
})
export class ToApiSourcePipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    return `http://openweathermap.org/img/wn/${value}@2x.png`;
  }

}
