import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertToC'
})
export class ConertToCPipe implements PipeTransform {

  transform(value: number, ...args: any[]): string {
    return (value - 273).toFixed(2);
  }

}
