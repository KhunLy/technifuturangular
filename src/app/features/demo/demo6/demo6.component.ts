import { Component, OnInit } from '@angular/core';
import { Observable, observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-demo6',
  templateUrl: './demo6.component.html',
  styleUrls: ['./demo6.component.scss']
})
export class Demo6Component implements OnInit {

  
  private _value : number;
  public get value() : number {
    return this._value;
  }
  public set value(v : number) {
    this._value = v;
  }

  
  private _value$ : Observable<number>;
  public get value$() : Observable<number> {
    return this._value$;
  }
  public set value$(v : Observable<number>) {
    this._value$ = v;
  }
  
  

  constructor() { }

  ngOnInit() {
    this.value = 0;
    let obs$ = new Observable<any>((observer) => {
      let compt = 0;
      setInterval(() => { 
        observer.next(++compt);
        if(compt == 10){
          observer.complete();
        } 
      } ,1000)
    });
// pipe permttra d'effectuer des operations sur le resulat de l'observable
// et de retourner un nouvel observable
    this.value$ = obs$.pipe(
        map(data => { return data*2 }), 
        filter(data => { return data % 7 != 0 })
      );
      // .subscribe(
      //   (data) => { this.value = data },
      //   (error) => { console.log(2,error); },
      //   () => { console.log(3,"FINI"); },
      // );

      //this.testFonction();
      //console.log("ceci est afficher apres l'appel de testFonction");

    
  }

  // async testFonction() {
  //   let obs2$ = new Observable<number>(observer => {
  //     setTimeout(() => {
  //       console.log("observer a démarré");
  //       observer.next(42); 
  //       observer.complete() 
  //     }, 2000);
  //   });

  //   let data = await obs2$.toPromise();
  //   console.log(data);
    
  // }

}
