import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo3',
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.scss']
})
export class Demo3Component implements OnInit {

  
  private _collapse : boolean;
  public get collapse() : boolean {
    return this._collapse;
  }
  public set collapse(v : boolean) {
    this._collapse = v;
  }
  

  constructor() { }

  ngOnInit() {
    this.collapse = false;
  }

  onClick() {
    this.collapse = !this.collapse;
  }

}
