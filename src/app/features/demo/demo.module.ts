import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRoutingModule } from './demo-routing.module';
import { DemoComponent } from './demo.component';
import { Demo1Component } from './demo1/demo1.component';
import { Demo2Component } from './demo2/demo2.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Demo3Component } from './demo3/demo3.component';
import { Demo4Component } from './demo4/demo4.component';
import { Demo5Component } from './demo5/demo5.component';
import { Demo6Component } from './demo6/demo6.component';
import { Demo7Component } from './demo7/demo7.component';
import { ToApiSourcePipe } from './_pipes/to-api-source.pipe';
import { ConertToCPipe } from './_pipes/conert-to-c.pipe';
import { CoreModule } from 'src/app/core/core.module';
import { Demo8Component } from './demo8/demo8.component';
import { NbDatepickerModule } from '@nebular/theme';


@NgModule({
  declarations: [DemoComponent, Demo1Component, Demo2Component, Demo3Component, Demo4Component, Demo5Component, Demo6Component, Demo7Component, ConertToCPipe, ToApiSourcePipe, Demo8Component],
  imports: [
    CommonModule,
    DemoRoutingModule,
    SharedModule,
    CoreModule,
    NbDatepickerModule,
  ]
})
export class DemoModule {

  constructor() {
    console.log("le module demo est chargé");
    
  }
 }
