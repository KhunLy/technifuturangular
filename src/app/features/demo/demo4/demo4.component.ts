import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo4',
  templateUrl: './demo4.component.html',
  styleUrls: ['./demo4.component.scss']
})
export class Demo4Component implements OnInit {

  
  private _contacts : string[];
  public get contacts() : string[] {
    return this._contacts;
  }
  public set contacts(v : string[]) {
    this._contacts = v;
  }
  

  constructor() { }

  ngOnInit() {
    this.contacts = ["Khun", "Nathan" , "Greg"];
  }

}
