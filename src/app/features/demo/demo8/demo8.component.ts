import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Register } from '../_models/register';
import { UserService } from 'src/app/core/_services/user.service';

@Component({
  selector: 'app-demo8',
  templateUrl: './demo8.component.html',
  styleUrls: ['./demo8.component.scss']
})
export class Demo8Component implements OnInit {

  public form: FormGroup

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      'email': new FormControl(null, Validators.compose([
        // Validators.email,
        // Validators.required,
        // Validators.pattern(/^a/)
      ])),
      'birthDate': new FormControl(new Date(), Validators.compose([
        this.ValidateBirthDate
      ])),
      'password': new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'confirmPassword': new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'condAgreement': new FormControl(false, Validators.requiredTrue)
    }, Validators.compose([
      this.ValidatePasswords
    ]))
  }

  ValidatePasswords(form: FormGroup) {
    let password = form.controls['password'].value;
    let confirm = form.controls['confirmPassword'].value;
    return password == confirm ? null : { 'pwd_missmatch': 'les passwords ne correspondent pas' } 
  }

  ValidateBirthDate(formControl: FormControl) {
    return formControl.value.getTime() > (new Date()).getTime() 
    ? { 'birthDate' : 'la date ne peut pas etre inf à ...' } : null;
  }

  onSubmit(objet: Register) {
    this.userService.register(objet).subscribe(data => {
      console.log(data);
    });
  }

}
