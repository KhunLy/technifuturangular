import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo2',
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.scss']
})
export class Demo2Component implements OnInit {

  
  private _maVariable : number;
  public get maVariable() : number {
    return this._maVariable;
  }
  public set maVariable(v : number) {
    this._maVariable = v;
  }

  
  private _maVariable2 : number;
  public get maVariable2() : number {
    return this._maVariable2;
  }
  public set maVariable2(v : number) {
    this._maVariable2 = v;
  }
  
  

  constructor() { }

  ngOnInit() {
    this.maVariable = 0;
    this.maVariable2 = 0;
  }

  onMouseOver() {
    this.maVariable++;
  }

  augmenter() {
    this.maVariable2++;
  }

  diminuer() {
    this.maVariable2--;
  }

}
