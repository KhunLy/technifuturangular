export interface Register{
    email: string;
    password: string;
    birthDate: Date;
}