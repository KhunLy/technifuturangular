import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo5',
  templateUrl: './demo5.component.html',
  styleUrls: ['./demo5.component.scss']
})
export class Demo5Component implements OnInit {

  
  private _prop1 : string;
  public get prop1() : string {
    return this._prop1;
  }
  public set prop1(v : string) {
    this._prop1 = v;
  }
  

  constructor() { }

  ngOnInit() {
    this.prop1 = "Salut";
  }

}
