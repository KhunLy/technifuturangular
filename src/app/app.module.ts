import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbSidebarModule, NbMenuModule, NbDialogModule, NbDatepickerModule } from '@nebular/theme';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { CharacterService } from './core/_services/character.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MarvelHashInterceptor } from './core/_interceptors/marvel-hash.interceptor';
import { LoaderInterceptor } from './core/_interceptors/loader.interceptor';
import { DemoModule } from './features/demo/demo.module';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    NbThemeModule.forRoot({ name: 'cosmic' }),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDialogModule.forRoot(),
    NbDatepickerModule.forRoot(),
    CoreModule,
  ],
  providers: [ 
    //{provide: CharacterService, useClass: CharacterService},
    // CharacterService
    { provide: HTTP_INTERCEPTORS, multi: true, useClass: MarvelHashInterceptor },
    { provide: HTTP_INTERCEPTORS, multi: true, useClass: LoaderInterceptor },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
