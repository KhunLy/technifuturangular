import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Md5 } from 'ts-md5';
import { environment } from 'src/environments/environment';

@Injectable()
export class MarvelHashInterceptor implements HttpInterceptor
{
    intercept(req: HttpRequest<any>, next: HttpHandler)
        : Observable<HttpEvent<any>> {
        let ts = (new Date()).getTime();
        let hash = Md5.hashStr(`${ts}${environment.marvelPriKey}${environment.marvelPubKey}`);
        
        let clone = req.clone({ setParams: {
            'ts' : ts.toString(),
            "apikey": environment.marvelPubKey,
            "hash": hash.toString()
        }, setHeaders: {
            //"Authorization": "Bearer " + token
        }});
        
        console.log(clone.urlWithParams);
            
        
        return next.handle(clone);
    }

}