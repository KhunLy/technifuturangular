import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoaderMediatorService } from '../_services/loader-mediator.service';
import { Injectable } from '@angular/core';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

    constructor(private mediator: LoaderMediatorService) {  }

    intercept(req: HttpRequest<any>, next: HttpHandler)
        : Observable<HttpEvent<any>> {
        //contacter mon mediator pour lui signaler que le loader doit s'afficher
        this.mediator.loaderState.next(true);
        return next.handle(req)
            .pipe(finalize(() => {
                //contacter le mediator pour cacher le loader
                this.mediator.loaderState.next(false);
            }));
    }
}