import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CharactersApi } from '../_models/characters-api';
import { CharacterService } from '../_services/character.service';

@Injectable()
export class CharacterResolverService implements Resolve<Observable<CharactersApi>> {

  constructor(private characterService: CharacterService) { console.log("je suis instancié");
   }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<CharactersApi> | Observable<Observable<CharactersApi>> | Promise<Observable<CharactersApi>> {
    let id = route.params["id"];
    return this.characterService.getById(id);
  }
}
