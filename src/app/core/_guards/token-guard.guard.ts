import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../_services/session.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TokenGuardGuard implements CanActivate {

  constructor(private session: SessionService){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.session.getToken() != null;
  }
  
}
