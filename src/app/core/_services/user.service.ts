import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Register } from 'src/app/features/demo/_models/register';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private client: HttpClient) { }

  register(model: Register): Observable<string>{
    return this.client
      .post<string>("https://testapi-883bb.firebaseio.com/Users.json", model);
  }

}
