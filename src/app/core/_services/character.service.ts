import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Md5 } from 'ts-md5';
import { CharactersApi } from '../_models/characters-api';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({providedIn: 'root'})
export class CharacterService{
  

  private readonly _endPoint 
    = "http://gateway.marvel.com/v1/public/characters";

  constructor(private httpClient: HttpClient) {
  }

  getAll(offset: number = 0): Observable<CharactersApi>{
    return this.httpClient.get<CharactersApi>(this._endPoint);
  }

  getById(id: number): Observable<CharactersApi>{
    return this.httpClient.get<CharactersApi>(this._endPoint + "/" + id);
  }
}
