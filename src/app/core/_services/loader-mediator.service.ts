import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class LoaderMediatorService {
    
    private _loaderState : Subject<boolean>;
    public get loaderState() : Subject<boolean> {
        return this._loaderState;
    }
    constructor() {
        this._loaderState = new Subject<boolean>();
    }
    
}