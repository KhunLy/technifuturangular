import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }

  login(email:string, password:string): Observable<string>{
    return new Observable<string>(o => {
      if(email == 'a@a' && password == 'test'){
        o.next('un super token');
      }
      else{
        o.error('message d\'erreur');
      }
      o.complete();
    });
  }
}
