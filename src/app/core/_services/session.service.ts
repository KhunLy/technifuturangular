import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  public sessionToken$: Subject<string>;

  constructor() { 
    this.sessionToken$ = new Subject<string>();
  }

  saveToken(token: string) {
    this.sessionToken$.next(token);
    localStorage.setItem('token', token);
  }

  getToken(): string {
    let token = localStorage.getItem('token');
    return token;
  }

  closeSession(){
    this.sessionToken$.next(null);
    localStorage.setItem('token', null);
  }
}
