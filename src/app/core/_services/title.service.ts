import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

  
  private _title$ : Subject<string>;
  public get title$() : Subject<string> {
    return this._title$;
  }
  public set title(v : string) {
    this._title$.next(v);
  }
  
  constructor() { 
    this._title$ = new Subject<string>();
  }
}
