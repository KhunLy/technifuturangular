import { Component, OnInit, OnDestroy } from '@angular/core';
import { CharacterService } from '../core/_services/character.service';
import { TitleService } from '../core/_services/title.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  public color = "danger";
  constructor(
    private charService: CharacterService,
    private titleService: TitleService
  ) { titleService.title = "Home" }

  ngOnInit() {
    setTimeout(() => {this.color = "success"}, 2000)
  }

  ngOnDestroy(): void {
    this.titleService.title = null;
  }

  onClose(ellapsedTime: number){
    console.log(ellapsedTime);
  }

}
