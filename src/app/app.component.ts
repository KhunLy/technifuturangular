import { Component } from '@angular/core';
import { TitleService } from './core/_services/title.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  //template: "<p>Hello Khun</p>",
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title;
  constructor(
    private titleService: TitleService
  ) {
    titleService.title$.subscribe(x => this.title = x);
  }
}
